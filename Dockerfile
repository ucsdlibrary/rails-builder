ARG RUBY_VERSION=2.6.5

FROM ruby:$RUBY_VERSION-alpine
ARG APP_NAME=blog
ARG APP_DB=sqlite3
ENV APP_NAME $APP_NAME
ENV APP_DB $APP_DB

RUN apk --no-cache upgrade && \
  apk add --no-cache \
  build-base \
  git \
  less \
  postgresql-dev \
  nodejs \
  nodejs-npm \
  sqlite-dev \
  tzdata \
  yarn \
  vim \
  && rm -rf /var/cache/apk/*

RUN gem install rails

COPY generate_rails_app.sh /generate_rails_app.sh
COPY templates/ /templates

WORKDIR /builder

ENTRYPOINT ["/bin/sh", "/generate_rails_app.sh"]
