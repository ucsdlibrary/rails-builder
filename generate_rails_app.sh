#!/usr/bin/env sh

rails new "$APP_NAME" --skip-test --skip-system-test --database="$APP_DB"

# copy default Dockerfile to project
cp /templates/Dockerfile "$APP_NAME"

# copy default docker entrypoint script
mkdir "$APP_NAME"/docker
cp /templates/docker-entrypoint.sh "$APP_NAME"/docker
cp /templates/docker-compose.yml "$APP_NAME"
cp /templates/.env.docker "$APP_NAME"
cp /templates/Makefile "$APP_NAME"
