# Rails Builder in Docker

The goal of this small project is to allow a developer to easily create a brand
new Rails app that is destined for development in a container context
(Docker,Podman).

In this context, the developer may not want or have the version of Ruby and/or
Rails installed on their local system that they would like to use for the new
application. Since development is taking place in a container context, this
should not matter.

This project allows one to easily get past the dependencies required to run
`rails new <project>` and get up and running.

It will also install starter templates for:

| Template/File | Description |
| ------------ | ------------ |
| `Dockerfile` | An Alpine linux based `Dockerfile` for application development |
| `Makefile` | A starter `Makefile` with some basic commands for development with `docker-compose` |
| `docker-entrypoint.sh` | An entrypoint script that allows you to customize the startup process for the container |
| `docker-compose.yml` | A starter `docker-compose.yml` with container in place for `postgresql` and `selenium`(system tests) |
| `.env.docker` | A starter environment file for the `docker-compose.yml` file |

## Requirements

* Docker

## Usage
- Pull the rails-builder image `docker pull registery.gitlab.com/ucsdlibrary/rails-builder:stable`

You may want to create a small shell function that you can call to simplify this
process. Here is one idea:

```
# Build a new rails app w/ docker
# expected args: app name, app database
new-rails-app(){
  local name="${1-blog}"
  local db="${2-postgresql}"
  docker pull registry.gitlab.com/ucsdlibrary/rails-builder:stable
  docker run --rm -it -e APP_NAME="$name" -e APP_DB="$db" --mount type=bind,source="$(pwd)",target=/builder registry.gitlab.com/ucsdlibrary/rails-builder:stable
  sudo chown -R "$UID":"$GID" "$name"
  cd "$name" || exit
}
```

Then, in a terminal, you might create a new app as follows:

```
new-rails-app my-app postgresql
```

Or, more manually:

- Create a directory you'd like to have your new project files in. Example
    `mkdir ~/projects/my-new-app`
- `cd ~/projects/my-new-app`
- `docker run --rm -it --mount type=bind,source="$(pwd)",target=/builder rails-builder`

If you want to customize the application being created, you can either use a
Docker [environment file][env-file] or use the `-e` option directly such as:

```
docker run --rm -it -e APP_NAME=my-new-app APP_DB=postgresql --mount type=bind,source="$(pwd)",target=/builder rails-builder
```

Finally, the files that are copied to your local machine may be owned by `root`.
To fix this you'll want something like:

```
# chown -R $UID:$GID my-new-app
```

## Future Goals/Ideas
- Expose more `rails new` options for args the developer can pass in
- Better database setup/integration
- Create a default/standard Helm chart for the project

[env-file]: https://docs.docker.com/engine/reference/commandline/run/#set-environment-variables--e---env---env-file
